<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" class="bx-core bx-no-touch bx-no-retina bx-chrome mdl-js"><head>
<meta charset="utf-8">
<title>Внутренний портал</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="index, follow">
<meta name="keywords" content="внутренний портал">
<meta name="description" content="Внутренний портал МИИГАиК">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="js/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.countdown.css"> 
<script type="text/javascript" src="js/jquery.plugin.js"></script> 
<script type="text/javascript" src="js/jquery.countdown.js"></script>
<script src="js/script.js"></script>
<body>
	<div class="header">
	
	</div>
	<div class="content">
		<div class="prepod">
			<table id='p'>
				<tr class="after">
					<td class='t-w-h'>СЛЕД.</td>
					<td class="afterp t-w-n"></td>
					<td class="after302 t-w"></td>
					<td class="after303 t-w"></td>
					<td class="after304 t-w"></td>
					<td class="after507 t-w"></td>
					<td class="after508 t-w"></td>
					<td class="after509 t-w"></td>
					<td class="after510 t-w"></td>
				</tr>
				<tr>
					<td rowspan="2">ТЕКУЩ.</td>
					<td rowspan="2" class="thisp"></td>
					<td class="th th302">302</td>
					<td class="th th303">303</td>
					<td class="th th304">304</td>
					<td class="th th507">507</td>
					<td class="th th508">508</td>
					<td class="th th509">509</td>
					<td class="th th510">510</td>
				</tr>
				<tr>

					<td class="this302"></td>
					<td class="this303"></td>
					<td class="this304"></td>
					<td class="this507"></td>
					<td class="this508"></td>
					<td class="this509"></td>
					<td class="this510"></td>
				</tr>
				<tr class="prev">
					<td>ПРЕД.</td>
					<td class="prevp"></td>
					<td class="prev302"></td>
					<td class="prev303"></td>
					<td class="prev304"></td>
					<td class="prev507"></td>
					<td class="prev508"></td>
					<td class="prev509"></td>
					<td class="prev510"></td>
				</tr>
			</table>
		</div>
		<div class="row">
			<div class="left">
				<div class="row">
					<div class="countdown">
						<h2></h2>
						<div id="time"></div>
					</div>
				</div>
			</div>
			
			<div class="right">
				<table id='t'>
					<tr>
						<td>
							<select id="selected">
								<option value='1'>1</option>
								<option value='2'>2</option>
								<option value='3'>3</option>
								<option value='4'>4</option>
								<option value='5'>5</option>
								<option value='6'>6</option>
								<option value='7'>7</option>
							</select>
						</td>
						<td>ПН</td>
						<td>ВТ</td>
						<td>СР</td>
						<td>ЧТ</td>
						<td>ПТ</td>
						<td>СБ</td>

					</tr>
					<tr class="up num" id="302">
						<td rowspan="2"><p>302</p></td>

					</tr>
					<tr class="down" id="302">

					</tr>
					<tr class="up num" id="303">
						<td rowspan="2"><p>303</p></td>

					</tr>
					<tr class="down" id="303">

					</tr>
					<tr class="up num" id="304">
						<td rowspan="2"><p>304</p></td>

					</tr>
					<tr class="down" id="304">

					</tr>
					<tr class="up num" id="507">
						<td rowspan="2"><p>507</p></td>
					</tr>
					<tr class="down" id="507">
					
					</tr>
					<tr class="up num" id="508">
						<td rowspan="2"><p>508</p></td>
					</tr>
					<tr class="down" id="508">
					
					</tr>
					<tr class="up num" id="509">
						<td rowspan="2"><p>509</p></td>
					</tr>
					<tr class="down" id="509">
					
					</tr>
					<tr class="up num" id="510">
						<td rowspan="2"><p>510</p></td>
					</tr>
					<tr class="down" id="510">
					
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>