﻿
<?php
header('Content-Type: charset=utf8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require('simple/simple_html_dom.php');
$url = [
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=4&audit=302',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=3&audit=303',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=2&audit=304',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=6&audit=507',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=7&audit=508',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=8&audit=509',
	'http://studydep.miigaik.ru/semestr/timetableau.php?a=5&audit=510'
];
$week = [
	'1' => 'Понедельник',
	'2' => 'Вторник',
	'3' => 'Среда',
	'4' => 'Четверг',
	'5' => 'Пятница',
	'6' => 'Суббота'
];
foreach($url as $line){
	$html = file_get_html($line);
	//echo $line;
	//echo $html;
	
	$table = $html->find('table[class=t]');
	//print_r($table) ;
	
	$i = 0;
	$s = 0;
	foreach($html->find('table[class=t]') as $table){
		
		foreach($table->find('tr') as $tr){
			//print_r($tr->find('td', 2)->innertext) ;
			//die;
			if(!empty($tr->find('td', 2)->innertext)){
				//print_r($tr->find('td', 2)->innertext) ;
				//	die;
				/*$td[0] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 0)->innertext);
				$td[1] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 1)->innertext);
				$td[2] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 2)->innertext);
				$td[3] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 3)->innertext);
				$td[4] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 4)->innertext);
				$td[5] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 5)->innertext);
				$td[6] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 6)->innertext);
				$td[7] = iconv('windows-1251', 'UTF-8//IGNORE', $tr->find('td', 7)->innertext);*/
				$td = [
					$tr->find('td', 0)->innertext,
					$tr->find('td', 1)->innertext,
					$tr->find('td', 2)->innertext,
					$tr->find('td', 3)->innertext,
					$tr->find('td', 4)->innertext,
					$tr->find('td', 5)->innertext,
					$tr->find('td', 6)->innertext,
					$tr->find('td', 7)->innertext
				];
				/*switch($td[0]){
					case 'Понедельник':
						$day = 'monday';
						break;
					case 'Вторник':
						$day = 'tuesday';
						break;
					case 'Среда':
						$day = 'wednesday';
						break;
					case 'Четверг':
						$day = 'thursday';
						break;
					case 'Пятница':
						$day = 'friday';
						break;
					case 'Суббота':
						$day = 'saturday';
						break;
				}*/
				switch($td[0]){
					case 'Понедельник':
						$day = '1';
						break;
					case 'Вторник':
						$day = '2';
						break;
					case 'Среда':
						$day = '3';
						break;
					case 'Четверг':
						$day = '4';
						break;
					case 'Пятница':
						$day = '5';
						break;
					case 'Суббота':
						$day = '6';
						break;
				}
				$number = preg_replace('/[^0-9]/', '', $td[1]);
				switch($td[2]){
					case 'верхняя':
						$week = 'up';
						break;
					case 'нижняя':
						$week = 'down';
						break;
				}
				$audit = substr($td[5], 0, 3);
				$array['table'][$audit][$week][$day][$number] = [
					'discipline' => $td[3],
					'teacher' => explode(" ", $td[4])
				];
			}
		}
	}
}
echo json_encode($array, JSON_UNESCAPED_UNICODE);