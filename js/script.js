var times = [
	{
		start: {hour: 09, minute: 00},
		stop: {hour: 10, minute: 30}
	},
	{
		start: {hour: 10, minute: 40},
		stop: {hour: 12, minute: 10}
	},
	{
		start: {hour: 12, minute: 50},
		stop: {hour: 14, minute: 20}
	},
	{
		start: {hour: 14, minute: 30},
		stop: {hour: 16, minute: 00}
	},
	{
		start: {hour: 16, minute: 10},
		stop: {hour: 17, minute: 40}
	},
	{
		start: {hour: 17, minute: 50},
		stop: {hour: 19, minute: 20}
	},
	{
		start: {hour: 19, minute: 30},
		stop: {hour: 21, minute: 00}
	},
	{
		start: {hour: 21, minute: 10},
		stop: {hour: 21, minute: 18}
	},
	{
		start: {hour: 23, minute: 25},
		stop: {hour: 23, minute: 55}
	},
	{
		start: {hour: 23, minute: 56},
		stop: {hour: 23, minute: 59}
	}
];
Date.prototype.getWeek = function (dowOffset) {
/*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

    dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};
function get_json(){
	var request = new XMLHttpRequest();
	request.open('JSON', 'https://swh.miigaik.ru/ukc/get_info.php', false);
	request.send( null );
    return JSON.parse(request.responseJSON);

}
function zan(number){
	$('#selected option[value='+number+']').attr('selected','selected');
	$.getJSON("https://swh.miigaik.ru/ukc/get_info.php", function(data){
		$.each(data.table ,function(audit, avalue){
			$('#t .td'+audit).remove()
			$.each(avalue.up ,function(day, dvalue){
				//console.log(dvalue[number])
				if(dvalue[number] !== undefined){
					$('#'+audit+'.up').append('<td style="background:#ff00008c" class="td'+audit+'"></td>');
				}else{
					$('#'+audit+'.up').append('<td style="background:#00800087" class="td'+audit+'"></td>');
				}
			})
			$.each(avalue.down ,function(day, dvalue){
				//console.log(dvalue[number])
				if(dvalue[number] !== undefined){
					$('#'+audit+'.down').append('<td style="background:#ff00008c" class="td'+audit+'"></td>');
				}else{
					$('#'+audit+'.down').append('<td style="background:#00800087" class="td'+audit+'"></td>');
				}
			})
		})
		
	});
	
}
function add_html(array, html, html_e, id, id_e){
	
	if(array !== undefined && array !== ''){
		$(id).html(array[html][0]);
	}else{
		$(id).html(html_e);
		$(id_e).css({"background-color": "#005800d1", "color": "#07ff07", "border": "1px solid #000"});
		if(id.indexOf('.this') == 3 && id.indexOf('.thisp') == -1){
			$(id).css({"background-color": "#00800087"});
		}
	}
}
function pre(number){
	var currentDateTime = new Date();
	var week = currentDateTime.getWeek();
	var day = currentDateTime.getDay();
	console.log(day);
	add_html('', '', number, '#p .thisp');
	add_html('', '', number+1, '#p .afterp');
	add_html('', '', number-1, '#p .prevp');
	$.getJSON("https://swh.miigaik.ru/ukc/get_info.php", function(data){
		$.each(data.table ,function(audit, avalue){
			if(week % 2){
				add_html(avalue.up[day][number], 'teacher', '', '#p .this'+audit, '#p .th'+audit)
				add_html(avalue.up[day][number+1], 'teacher', '', '#p .after'+audit, '')
				add_html(avalue.up[day][number-1], 'teacher', '', '#p .prev'+audit, '')
			}else{
				add_html(avalue.down[day][number], 'teacher', '', '#p .this'+audit, '#p .th'+audit)
				add_html(avalue.down[day][number+1], 'teacher', '', '#p .after'+audit, '')
				add_html(avalue.down[day][number-1], 'teacher', '', '#p .prev'+audit, '')
			}
		})
	})
}

function refresh_p(number, pereriv){
	zan(number);
	pre(number);
	if(pereriv){
		$('.countdown h2').html('Конец перерыва через')
	}else{
		$('.countdown h2').html('Конец пары через')
	}
}
function timer(refresh){
	var newYear = new Date(); 
	$.each(times ,function(param, value){
		if(newYear.getHours() >= value.start.hour){
			if(newYear.getMinutes() >= value.start.minute || newYear.getHours() > value.start.hour){
				if(newYear.getHours() <= value.stop.hour){
					if(newYear.getMinutes() < value.stop.minute || newYear.getHours() < value.stop.hour){
						date = new Date(newYear.getFullYear(), newYear.getMonth(), newYear.getDate(), value.stop.hour, value.stop.minute, 0, 0);
						date_start = new Date(newYear.getFullYear(), newYear.getMonth(), newYear.getDate(), value.start.hour, value.start.minute, 0, 0);
						if(refresh){
							$('#time').countdown('option', {until: date,   format: 'HMS', description: param+1 +' Пара', onExpiry: timer_refresh, compact: true});
						}else{
							$('#time').countdown({until: date,   format: 'HMS', description: param+1 +' Пара', onExpiry: timer_refresh, compact: true});
						}
						refresh_p(param+1, 0)
					}else if(times.length - 1 >= param + 1){
						if(newYear.getHours() <= times[param+1].start.hour && newYear.getMinutes() <= times[param+1].start.minute){
							console.log('33')
							var minute = times[param+1].start.minute;
							var hour = times[param+1].start.hour ;
							/*if(minute >= 60){
								minute = minute - 60;
								hour = hour + 1;
							}*/
							date = new Date(newYear.getFullYear(), newYear.getMonth(), newYear.getDate(), hour, minute, 0, 0);
							if(refresh){
								$('#time').countdown('option', {until: date, format: 'HMS', description: ' Перерыв', onExpiry: timer_refresh, compact: true});
							}else{
								$('#time').countdown({until: date, format: 'HMS', description: ' Перерыв', onExpiry: timer_refresh, compact: true});
							}
						}
						refresh_p(param+2, 1)
					}
				}
			}
		}
		if(param > 6){
			
		}
	});
}
function timer_refresh(){
	timer(1);
	//console.log(1);
	//$('#time').html('1');
}
//console.log(times[0].start.hour);
$(function(){
	$('#selected').on('change', function() {
	  zan(this.value);
	});
	timer();
	/*zan();
	$.getJSON("https://swh.miigaik.ru/ukc/get_info.php", function(data){
		var array = data;
		var table = '<table>';
		$.each(array.table ,function(audit, avalue){
			day_html = '<tr>';
			$.each(avalue.up ,function(day, dvalue){
				day_html +=	'<td>'+day+'</td>';
				//console.log(dvalue[1]);
				if(dvalue[1] != undefined){
					$('#'+audit+'.up').append('<td style="background:red"></td>');
				}else{
					$('#'+audit+'.up').append('<td style="background:green"></td>');
				}
				/*for (var para = 1; para < 8; para++) {
				   if(dvalue[para]){
					   $('#'+audit+'.up').append('<td style="background:#000"></td>');
					   console.log(1);
				   }else{
					    $('#'+audit+'.down').append('<td style="background:#001"></td>');
						console.log(0);
				   }
				}
			});
			day_html += '</tr>';
			$('#t').append(day_html);
		});
		var table = '</table>';
	});*/
	
	
});
